#!/bin/bash
# script_17.sh - Por Rafael Aquino
# Condicionales: if 

x=100
y=10
# uso de -gt --> mayor que
if [[ $x -gt $y ]]
then
  echo "x=$x es mayor que y=$y"
fi   

a=20;b=30
# uso de -lt --> menor que
if [[ $a -lt $b ]]; then
  echo "a=$a es menor a b=$b"
fi

N1=500;N2=500
# uso de -eq --> Igual que
if [[ $N1 -eq $N2 ]]; then
  echo "N1=$N1 es igual a N2=$N2"
fi

num_1=123;num_2=321
# uso de -ne --> diferente
if [[ $num_1 -ne $num_2 ]]; then
  echo "num_1=$num_1 es diferente a num_2=$num_2"
fi

