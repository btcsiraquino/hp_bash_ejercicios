#!/bin/bash
# script_22.sh - Por: Rafael Aquino
# Comandos lógicos: or: ||
x=10;y=200
if [[ $x -eq 10 ]] || [[ $y -eq 20 ]]; then
  echo "or - Se cumplen la primera condicion"
fi

x=10000;y=20
if [[ $x -eq 10 ]] || [[ $y -eq 20 ]]; then
  echo "or - Se cumple la segunda condicion"
fi

x=10000;y=40
if [[ $x -eq 10 ]] || [[ $y -eq 20 ]]; then
  echo "Aqui no entra no se cumple ni la 1era ni 2da"
else
  echo "Aqui entra"
fi



