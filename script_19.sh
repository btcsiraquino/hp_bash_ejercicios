#!/bin/bash
# script_19.sh: Por Rafael Aquino
# Condicionales: if-elif-else 
x=1000
if [[ $x -gt 500 ]]
then
  echo "Pasa por aqui - primer if porque X=1000"
elif [[ $x -eq 100 ]]; then
  echo "Entro a elif "
else
  echo "Estamos en caso contrario - else" 
fi   

x=100
if [[ $x -gt 500 ]]
then
  echo "Pasa por aqui - primer if"
elif [[ $x -eq 100 ]]; then
  echo "Entro a elif porque x=100"
else
echo "Estamos en caso contrario - else" 
fi

x=-4
if [[ $x -gt 500 ]]
then
  echo "Pasa por aqui - primer if"
elif [[ $x -eq 100 ]]; then
  echo "Entro a elif "
else
  echo "Estamos en caso contrario - else x=-4" 
fi

