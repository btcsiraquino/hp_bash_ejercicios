#!/bin/bash
# script_52.sh Por: Rafael Aquino
# Ciclo While: Uso de contador y acumulador 

# Realizar la sumatoria de los numeros 1,2,3,4,5

cont=1
sum=0
while [[ $cont -lt 6 ]];
do
  (( sum=sum+$cont ))
  (( cont++ ))
done
echo " La suma de los primeros cinco numeros naturales es: $sum"
#...................................
