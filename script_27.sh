#!/bin/bash
# Script_27.sh - Por: Rafael Aquino
# Pruebas de Archivos - Determinar si un archivo es un enlace simbólico
# Para correr el script: bash script_27.sh <nombredearchivo>
# crea un enlace simbólico para realizar el ejercicio
echo "Determina si un archivo es un enlace simbólico"
if [[ -h "$1" ]]; then
  echo "El archivo $1 es un enlace simbólico"
else
  echo "No es un enlace simbólico el achivo $1."
fi
