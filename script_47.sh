#!/bin/bash
# script_47.sh - Por: Rafael Aquino
# for in: sentencia --> continue
# 

for (( i=1; i<=7; i++ )); 
do
  if [[ $i -eq 2 ]] || [[ $i -eq 5 ]]; then
  continue
  fi
  echo " Valor de i: " $i
done
