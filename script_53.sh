#!/bin/bash
# script_53.sh Por: Rafael Aquino
# Ciclo until 

# Realizar la sumatoria de los numeros 1,2,3,4,5

cont=1
sum=0
until [[ $cont -gt 5 ]];
do
  (( sum=sum+$cont ))
  (( cont++ ))
done
echo " La suma de los primeros cinco numeros naturales es: $sum"
#...................................
