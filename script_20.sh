#!/bin/bash
# script_20.sh: Por Rafael Aquino
# Condicionales: case 
x=100
case $x in
  100)
	echo "X = 100"
	;;
  200)
	echo "X = 200"
	;;
  *)
	echo "Cualquier valor"
  esac
x=200
case $x in
   100)
        echo "X = 100"
        ;;
   200)
        echo "X = 200"
        ;;
   *)
        echo "Cualquier valor"
   esac
x=5
case $x in
  100)
        echo "X = 100"
        ;;
  200)
        echo "X = 200"
        ;;
  *)
        echo "Cualquier valor, aqui x=$x"
  esac
