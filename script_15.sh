#!/bin/bash
# script_15.sh - Por: Rafael Aquino
# Listas - Por expansión

echo "Lista 1..5"
list_1=({1..5})
echo 'Lista completa de 1..5: ' ${list_1[@]}
echo "Primer elemento de 1..5: ${list_1[0]}"
echo "Último elemento de 1..5: ${list_1[-1]}"
echo " "
echo "Lista a..z"
list_2=({a..z})
echo 'Lista completa de a..z: ' ${list_1[@]}
echo "Primer Elemento de a..z: ${list_2[0]}"
echo "Último Elemento de a..z: ${list_2[-1]}"
echo " "
echo "Lista A..Z"
list_3=({A..Z})
echo 'Lista completa: ' ${list_3[@]}
echo "Primer elemento de A..Z: ${list_3[0]}"
echo "Último Elemento de A..Z: ${list_3[-1]}"

