#!/bin/bash
# script_55.sh Por: Rafael Aquino
# Ciclo infinito con while (2) 
c=1
while true
do
  (( c++ ))
  echo " Son $c ciclos "
  echo "Ctrl + c, para salir"
done
#...................................
