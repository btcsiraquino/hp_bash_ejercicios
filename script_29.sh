#!/bin/bash
# Script_29.sh - Por: Rafael Aquino
# Comparación de cadenas - Cadenas diferentes
CADENA1='palabra1'
CADENA2='palabra2'
echo "Determina si dos cadenas son diferentes"
if [[ $CADENA1 != $CADENA2 ]]; then
  echo "Las dos cadenas son diferentes"
fi
