#!/bin/bash
# script_13.sh - Por: Rafael Aquino
#Listas - String
palabras=('Hola' 'saludos' 'chao')
echo "${palabras[0]}"
echo "${palabras[1]}"
echo "${palabras[2]}"

echo 'Lista completa: ' ${palabras[@]}
echo 'Lista completa: ' ${palabras[*]}
echo 'Número de elementos:' ${#palabras[@]}
