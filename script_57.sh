#!/bin/bash
# script_57.sh Por: Rafael Aquino
# Ciclo infinito con for  
c=1
for (( ; ; ))
do
  (( c++ ))
  echo " Son $c ciclos "
  echo "Ctrl + c, para salir"
done
#...................................
