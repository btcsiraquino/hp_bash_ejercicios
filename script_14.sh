#!/bin/bash
# script_14.sh - Por: Rafael Aquino
# Listas - números 

numeros=(10 20 30 40)
echo "${numeros[0]}"
echo "${numeros[1]}"
echo "${numeros[2]}"
echo "${numeros[3]}"

echo 'Lista completa: ' ${numeros[@]}
echo 'Lista completa: ' ${numeros[*]}
echo 'Número de elementos:' ${#numeros[@]}
