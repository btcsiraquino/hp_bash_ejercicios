#!/bin/bash
# script_41.sh - Por: Rafael Aquino
# Contador -  Compacto o reducido --> cont++

cont=0
for i in {1..5}; 
do
  echo " Este es i: $i"
  (( cont++ ))
  echo " Este es el contador " $cont
done
