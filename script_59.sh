#!/bin/bash
# script_59.sh Por: Rafael Aquino
# continue y break con until  
c=0
until [[ $c -gt 100 ]];
do
  (( c++ ))  
  if [[ $c -eq 2 ]]; then
    continue
  fi
  if [[ $c -eq 11 ]]; then
    break
  fi
  echo " Valor contador $c "
done
#...................................
