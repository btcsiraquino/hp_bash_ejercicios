#!/bin/bash
# script_49.sh - Por: Rafael Aquino
# Operadores Aritmeticos

a=2; b=1

echo "Suma"
suma=$(( a + b ))
echo "La suma de $a + $b es igual a:" $suma

echo "Resta"
resta=$(( a - b ))
echo "La resta de $a - $b es igual a:" $resta  

echo "Multiplicacion"
mult=$(( a * b ))
echo "La multiplicacion de $a * $b es igual a:" $mult  

echo "Division"
div=$(( a / b ))
echo "La division de $a / $b es igual a:" $div  

echo "Resto"
res=$(( a % b ))
echo "El resto de $a % $b es igual a:" $res  

echo "Potenciacion"
pot=$(( a ** b ))
echo "Potencia de $a ** $b es igual a:" $pot  
