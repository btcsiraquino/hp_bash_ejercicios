#!/bini/bash
# script_72.sh Por: Rafael Aquino
# Funciones - Ejercicios 

# Declaración
mi_funcion_suma () {
  n1=$1;n2=$2
  SUMA=$(( n1 + n2 ))
  echo "La suma de $n1 + $n2 es: $SUMA"
}

# LLamada de la función
mi_funcion_suma 4 7 

#.................................
