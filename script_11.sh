#!/bin/bash
# script_11.sh
# Un solo comentario

: 'Comentario de varias
lineas por ejemplo
este tiene cuatro
lineas
' 

:
<<!
Comentario de
multilinea. Son
tres lineas
!

<<Comento
Otra forma de comentario es usando el 'Heredoc'
donde podemos cambiar la palabra que contiene el comentario.
XXX Aquí use: Comento.
Fin del comentario 
Comento

# Encontrarás multiples comentarios
# dentro del sistema que puedes hacer, 
# igual funciona.

echo "Hola" 
