#!/bin/bash
# script_54.sh Por: Rafael Aquino
# Ciclo infinito con while 
c=1
while :
do
  (( c++ ))
  echo " Son $c ciclos "
  echo "Ctrl + c, para salir"
done
#...................................
