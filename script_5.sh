#!/bin/bash
# Shells del Sistema
echo "Mostrando los Shells del Sistema"
cat /etc/shells
echo -e "\nDistintas formas de ubicar nuestro Shell: /bin/bash"
echo -e "\nComando usado: echo $SHELL"
echo $SHELL
echo -e "\nComando usado: env | head -n 1"
env | head -n 1
echo -e "\nComando usado: printenv SHELL"
printenv SHELL
echo -e "\nComando usado: printenv | head -n 1"
printenv | head -n 1


