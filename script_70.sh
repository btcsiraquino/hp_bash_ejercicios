#!/bini/bash
# script_70.sh Por: Rafael Aquino
# Funciones - Argumentos

# Declaración
mi_funcion () {
  echo "Segunda función en bash con argumentos"
  echo "Primer Argumento $1"
  echo "Segundo Argumento $2"
}

# LLamada de la función con argumentos
mi_funcion 35 78

#.................................
