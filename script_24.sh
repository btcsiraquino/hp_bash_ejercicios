#!/bin/bash
# Script_24.sh - Por: Rafael Aquino
# Para correr el script: bash script_24.sh <nombredearchivo>
# Ejemplo: bash script_24.sh script_23.sh

echo "Determina si un archivo existe"
if [[ -f "$1" ]]; then
  echo "El archivo $1 existe en este directorio"
else
  echo "No existe en este directorio el archivo $1"
fi
