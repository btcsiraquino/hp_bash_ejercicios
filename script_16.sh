#!/bin/bash
# scrip_16.sh - Por: Rafael Aquino
# Listas - Agregando elementos - Modificando - Eliminando 
colores=('amarillo' 'azul' 'rojo')
echo "Colores"
echo "${colores[@]}"
echo "Número de elementos: " ${#colores[@]}
echo " "
echo "Agregando elemento al fina"
colores[${#colores[@]}+1]='Verde'
echo "${colores[@]}"
echo "Otra forma Agregar al Final"
colores=("${colores[@]}" "Morado")
echo "${colores[@]}"
echo "Agregar elemento al comienzo"
colores=("blanco" "${colores[@]}")
echo "${colores[@]}"
echo "Número de elementos: " ${#colores[@]}
echo " "
echo "Eliminando amarillo que esta en la posición 1"
unset colores[1]
echo "${colores[@]}"
echo "Número de elementos: " ${#colores[@]}
echo " "
echo "Modificando: cambiar blanco por amarillo2"
colores[0]="amarillo2"
echo "${colores[@]}"
echo " "
echo "Eliminando toda la lista"
unset colores
echo "${colores[@]}"
echo "${colores[1]}"
