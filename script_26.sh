#!/bin/bash
# Script_26.sh - Por: Rafael Aquino
# Pruebas de Archivos - Determina si un archivo es un Directorio
# Para correr el script: bash script_26.sh <nombredearchivo>
# Ejemplo: bash script_26.sh test
# crea y elimina un directorio denominado test para el ejercicio
echo "Determina si un archivo es un directorio"
if [[ -d "$1" ]]; then
  echo "El archivo $1 es un directorio"
else
  echo "No es un directorio el achivo $1."
fi
