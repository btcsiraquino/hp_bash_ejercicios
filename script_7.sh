#!/bin/bash
#script_7.sh
echo Mostrando Variables
VAR1="Palabra"
VAR2="Esta es una Frase"
echo "Esta es VAR1: ${VAR1}" #Primera Forma 
echo "Esta es VAR2: " ${VAR2} #Segunda forma
V="Nombre"
echo "${V}"
echo ${V}
#..............................

# En un ejercicio anterior
# Concatenando variables
echo "Concatenando variables"
echo "Script Saludo"
V1='Hola'
V2='Mundo'
V3="${V1} ${2}"
echo "${V3}"
