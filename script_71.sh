#!/bini/bash
# script_71.sh Por: Rafael Aquino
# Funciones - Argumentos (Variables especiales)

# Declaración
mi_funcion () {
  echo "Tercera función en bash con argumentos (variables especiales)"
  echo "Primer Argumento $1"
  echo "Segundo Argumento $2"
  echo "Cantidad de argumentos $#"
  echo "Todos los argumentos $@"
}

# LLamada de la función
mi_funcion 35 78

#.................................
