#!/bin/bash
# script_56.sh Por: Rafael Aquino
# Ciclo infinito con until  
c=1
until false
do
  (( c++ ))
  echo " Son $c ciclos "
  echo "Ctrl + c, para salir"
done
#...................................
