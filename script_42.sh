#!/bin/bash
# script_42.sh - Por: Rafael Aquino
# Contador -  Compacto o reducido --> cont--

cont=10
for i in {1..5}; 
do
  echo " Este es i: $i"
  (( cont-- ))
  echo " Este es el contador " $cont
done
