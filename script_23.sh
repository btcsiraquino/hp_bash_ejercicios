#!/bin/bash
# script_23.sh - Por: Rafael Aquino
# Comandos lógicos: negación !
x=10
if [[ $x -eq 10 ]]; then
  echo "Se cumplen la condición, sin negación"
else
  echo "Entra aqui por la negación"
fi

#Negación
x=10
if ! [[ $x -eq 10 ]]; then
  echo "Se cumplen la condición"
else
  echo "Entra aqui por la negación"
fi
