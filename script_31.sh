#!/bin/bash
# Script_31.sh - Por: Rafael Aquino
# Comparación de cadenas - Cadena con valor, tiene contenido
CADENA1='cadena llena'
echo "Determina si la cadena de caracteres tiene un valor"
if [[ -n $CADENA1 ]]; then
  echo 'La cadena de caracteres tiene un valor'
fi
