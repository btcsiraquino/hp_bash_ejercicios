#!/bin/bash
# Script_28.sh - Por: Rafael Aquino
# Comparación de cadenas - Cadenas iguales
CADENA1='Hola'
CADENA2='Hola'
# CADENA3='Saludos'
echo "Determina si dos cadenas son iguales"
if [[ $CADENA1 = $CADENA2 ]]; then
  echo "Las dos cadenas son iguales"
else
  echo "Las cadenas no son iguales"
fi
