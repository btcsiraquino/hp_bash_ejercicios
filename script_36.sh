#!/bin/bash
# script_36.sh - Por: Rafael Aquino
# for: Rango
echo 'Rango del 1 al 4 - Ascendente'
for i in {1..4};
do
  echo $i
done
echo 'Rango del 4 al 1 - Descendente'
for i in {4..1};
do
  echo $i
done
echo 'Con incremento'
for i in {2..10..2};
do
  echo $i
done
echo 'De a..z'
for j in {a..z};
do
  echo -n $j
done
echo ' '

echo 'De A..Z'
for x in {A..Z};
do
echo -n $x
done
echo ' '
