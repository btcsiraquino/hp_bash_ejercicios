#!/bin/bash
# script_40.sh - Por: Rafael Aquino
# Contador -  Compacto o reducido --> cont++

echo "Forma reducida o compacta"
(( cont++ ))
echo "cont tiene valor $cont"
(( cont++ ))
echo "cont tiene valor $cont"
(( cont++ ))
echo "cont tiene valor $cont"

