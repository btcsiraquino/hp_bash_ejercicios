#!/bin/bash
# script_18 - Por Rafael Aquino
# Condicionales: if-else : Por Rafael Aquino

x=3
y=7

if [[ $x -gt $y ]]
then
  echo "x=$x es mayor que y=$y"
else
  echo "x=$x No es mayor que y=$y"
fi   

