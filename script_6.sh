#!/bin/bash
# Script_6

<<Pautas
Crear el archivo documento.txt
Agregar una frase a documento.txt, con el comando cat.
Visualizar documento.txt
Agregar una segunda frase a documento.txt, con el comando cat
Visualizar documento.txt
Visualizar la expresión FIN DEL SCRIPT.
Pautas

# creación de documento desde un Script de Bash
touch documento.txt
echo "Primera Frase" > documento.txt
cat documento.txt
echo "Segunda Frase" >> documento.txt
cat documento.txt
echo "FIN DEL SCRIPT"
