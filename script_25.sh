#!/bin/bash
# Script_25.sh - Por: Rafael Aquino
# Pruebas de Archivos - Archivo vacío o con contenido
# Para correr el script: bash script_25.sh <nombredearchivo>
# Ejemplo: bash script_25.sh script_23.sh
touch vacio.txt
echo "Determina si un archivo esta vacío o contiene información"
if [[ -s "$1" ]]; then
  echo "El archivo $1 no esta vacío"
else
  echo "Esta vació el archivo $1. No contiene informacion."
fi
