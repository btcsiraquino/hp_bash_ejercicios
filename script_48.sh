#!/bin/bash
# script_48.sh - Por: Rafael Aquino
# for in: sentencia --> break

for (( i=1; i<=7; i++ )); 
do
  if [[ $i -eq 5 ]]; then
  echo "Hasta aqui"
  break
  fi
  echo "Valor de i: " $i
done
