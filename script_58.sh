#!/bin/bash
# script_58.sh Por: Rafael Aquino
# continue y break con while  
c=0
while [[ $c -le 100 ]];
do
  (( c++ ))  
  if [[ $c -eq 2 ]]; then
    continue
  fi
  if [[ $c -eq 11 ]]; then
    break
  fi
  echo " Valor contador $c "
done
#...................................
