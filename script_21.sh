#!/bin/bash
# script_21.sh - Por: Rafael Aquino
# Comandos lógicos: and: &&  
x=10;y=20
if [[ $x -eq 10 ]] && [[ $y -eq 20 ]]; then
  echo "Se cumplen las dos condiciones"
else
  echo "No se cumple una de las condiciones"
fi

x=10;y=200
if [[ $x -eq 10 ]] && [[ $y -eq 20 ]]; then
  echo "Se cumplen las dos condiciones"
else
  echo "No se cumple una de las condiciones"
fi
